const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

const api = require("./api");

const app = express();

app.use(
  cors({
    origin: "*"
  })
);
app.use(bodyParser.json());

app.get("/api/instruments", function(req, res) {
  api.get("/instrument/active", function(body) {
    res.send(body);
  });
});

app.get("/api/bucket/:symbol", function(req, res) {
  const symbol = req.params.symbol;
  api.get(
    `/trade/bucketed?binSize=1m&partial=false&count=100&reverse=true&symbol=${symbol}`,
    function(body) {
      res.send(body);
    }
  );
});

app.get("/api/orders", function(req, res) {
  api.get("/order?reverse=true", function(body) {
    res.send(body);
  });
});

app.post("/api/order", function(req, res) {
  api.post("/order", req.body, function(data) {
    res.send(data);
  });
});

app.listen(3000, function() {
  console.info("Server is started...");
});
