const request = require("request");
const crypto = require("crypto");

const apiKey = "JHeWb9Rr2FnuMia8B1jgGFZ3";
const apiSecret = "Jlm4H-wZIaqd82RKVv58v63p62qp1OqSzvHz-TvZwhXsGGNm";

const baseApiUrl = "https://testnet.bitmex.com";

function get(url, callback) {
  request(getAuthHeaders(`/api/v1${url}`, "GET"), function(
    error,
    response,
    body
  ) {
    callback(body);
  });
}

function post(url, data, callback) {
  request(getAuthHeaders(`/api/v1${url}`, "POST", data), function(
    error,
    response,
    body
  ) {
    callback(body);
  });
}

function getAuthHeaders(path, method, data = {}) {
  const expires = Math.round(new Date().getTime() / 1000) + 60;
  const postBody = JSON.stringify(data);

  const signature = crypto
    .createHmac("sha256", apiSecret)
    .update(method + path + expires + postBody)
    .digest("hex");

  const headers = {
    "content-type": "application/json",
    Accept: "application/json",
    "X-Requested-With": "XMLHttpRequest",
    "api-expires": expires,
    "api-key": apiKey,
    "api-signature": signature
  };

  return {
    headers: headers,
    url: baseApiUrl + path,
    method,
    body: postBody
  };
}

module.exports = {
  get,
  post
};
