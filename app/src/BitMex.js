const baseUrl = "wss://testnet.bitmex.com/realtime";

export default class Client {
  constructor(sub, cb) {
    this.ws = new WebSocket(baseUrl + `?subscribe=${sub}`);
    this.ws.onmessage = cb;
  }

  close() {
    this.ws.close();
  }
}
