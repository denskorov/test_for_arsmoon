import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import axios from "axios";
import VueAxios from "vue-axios";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "bootstrap/dist/css/bootstrap.css";

import App from "./App.vue";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);

const instance = axios.create({
  baseURL: "http://localhost:3000/api/"
});

Vue.use(VueAxios, instance);

new Vue({
  render: h => h(App)
}).$mount("#app");
